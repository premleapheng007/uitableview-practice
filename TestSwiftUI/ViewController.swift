//
//  ViewController.swift
//  TestSwiftUI
//
//  Created by JONNY on 8/27/1399 AP.
//

import UIKit


struct MobileBrand {
    var brandName: String
    var modelName: [String]
}

class ViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var mobileBrand = [MobileBrand]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        

        tableView.register(MyTableViewCell.self, forCellReuseIdentifier: "cell")
        
        tableView.register(UINib(nibName: "MyTableViewCell", bundle: nil), forCellReuseIdentifier: "cell")
        
        mobileBrand.append(MobileBrand.init(brandName: "Apple", modelName: ["iPhone 7+","iPhone 11", "iPhone 11 Pro"]))
        mobileBrand.append(MobileBrand.init(brandName: "Samsung", modelName: ["Samsung Galaxy Note 9", "Samsung Galaxy Note 9+", "Samsung Galaxy Note 10"]))
        
    }
    
  
}

extension ViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return mobileBrand.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return mobileBrand[section].modelName.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.textLabel?.text = mobileBrand[indexPath.section].modelName[indexPath.row]
        return cell
    }

    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        mobileBrand[section].brandName
    }
    
 
    
}

